// Package containing diverses utilities

package utils

import(
    "bufio"
    "fmt"
    "os"
    "time"
)

func Spinner(stage string, delay int, quit chan struct{}) {
    for {
        select {
        case <-quit:
            return
        default:
            for _, r := range `-\|/` {
                fmt.Printf("\r%v... : %c", stage, r)
                time.Sleep( time.Duration(delay) * time.Millisecond)
            }
        }
    }
}

func GetInput(msg string) string {
    r := bufio.NewScanner(os.Stdin)
    for {
        fmt.Printf("\r%v : ", msg)
        r.Scan()
        switch inp := r.Text(); inp {
        default:
            return r.Text()
        }
    }
}
